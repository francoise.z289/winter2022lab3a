public class Application{
	public static void main(String[]args){
		Student stu1=new Student();
		Student stu2=new Student();
		
		stu1.name="Francoise";
		stu1.assignment=true;
		stu1.rScore=30;
		
		stu2.name="A";
		stu2.rScore=26;
		
		//System.out.println("Stu1:"+stu1.name+"\t"+stu1.assignment+"\t"+stu1.rScore);
		//System.out.println("Stu2:"+stu2.name+"\t"+stu2.assignment+"\t"+stu2.rScore);
		
		//stu1.checkAssignment();
		//stu2.checkAssignment();
		
		Student[] section4=new Student[3];
		
		section4[0]=stu1;
		section4[1]=stu2;
		
		//System.out.println(section4[0].name);
		
		section4[2]=new Student();
		section4[2].name="B";
		section4[2].rScore=28;
		System.out.println(section4[2].name);
	}
}