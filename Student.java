public class Student{
	public String name;
	public boolean assignment;
	public double rScore;
	
	
	public void checkAssignment(){
		if(this.assignment){
			System.out.println(this.name+" already do the assignment.");
		}else{
			System.out.println(this.name+" didn't do the assignment.");
		}
	} 
	
	public void goToClass(){
		System.out.println(this.name+" go to Java Class");
	}
}